#include <stdio.h>
#include <math.h>

//查找100以内的最大素数

int main(void)
{
	int i, j;
    int max = 0;
    int is_prime;

    for (i = 2; i <= 100; i++) { // 从2开始，因为1不是素数
        is_prime = 1; // 假设当前数字是素数

        for (j = 2; j <= sqrt(i); j++) { // 只需检查到sqrt(i)
            if (i % j == 0) {
                is_prime = 0; // 发现因子，当前数字不是素数
                break;
            }
        }

        if (is_prime) {
            max = i; // 更新最大素数
        }
    }

    printf("max = %d\n", max);

	return 0;
}